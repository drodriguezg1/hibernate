package act2;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import act1.Tamayito;

@Entity
@Table(name="Alimentos")

public class Aliment {
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idComida")
	private int id;
	@Column(name="nom", length=50, nullable=false) 
	private String nom;
	@Column(name="descripcion", length=50, nullable=false) 
	private String descripcion;
	@Column(name="ValorNutricional", nullable=false, columnDefinition = "double(6,2)")
	private double nutricion = 10.00;
	@OneToMany(mappedBy = "aliment")
	private Set<Tamayito> tamis = new HashSet<Tamayito>();
	
	
	public Set<Tamayito> getTamis() {
		return tamis;
	}


	public void setTamis(Tamayito t) {
		tamis.add(t);
	}

	public Aliment() {
		super();
	}


	public int getId() {
		return id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public double getNutricion() {
		return nutricion;
	}


	public void setNutricion(double nutricion) {
		this.nutricion = nutricion;
	}


	@Override
	public String toString() {
		return "Aliment [id=" + id + ", nom=" + nom + ", descripcion=" + descripcion + ", nutricion=" + nutricion + "]";
	}
	
	
	
	
}
