package act2;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import act1.Tamayito;

@Entity
@Table(name="Juguetes")
public class Joguina {
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idComida")
	private int id;
	@Column(name="nom", length=50, nullable=false) 
	private String nom;
	@Column(name="descripcion", length=100, nullable=false) 
	private String descripcion;
	@Column(name="NivelDiversion")
	private int diversion = 5;
	@OneToOne(mappedBy = "jogo", cascade = CascadeType.PERSIST)
	private Tamayito tami;




	public Joguina() {
		super();
	}
	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getDiversion() {
		return diversion;
	}
	public void setDiversion(int diversion) {
		this.diversion = diversion;
	}
	public int getId() {
		return id;
	}

	public Tamayito getTami() {
		return tami;
	}

	public void setTami(Tamayito tami) {
		this.tami = tami;
	}


	@Override
	public String toString() {
		return "Joguina [id=" + id + ", nom=" + nom + ", descripcion=" + descripcion + ", diversion=" + diversion + "]";
	}
	
	
	
	
}
