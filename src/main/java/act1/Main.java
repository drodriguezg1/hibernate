package act1;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;



public class Main {
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	

	public static synchronized SessionFactory getSessionFactory() 
	{
		try 
		{
			if(sessionFactory == null) 
			{
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
				Metadata metaData  = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
			
		} 
		catch (Throwable ex) 
		{
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static void main(String[] args) {

		
		session = getSessionFactory().openSession();
		session.beginTransaction();
		
		Tamayito tami = new Tamayito();
		tami.setNom("Santiagot");
		tami.setDescripcion("tengo touret");
		tami.setEtapa(Etapa.Baby);
		
		Tamayito tami2 = new Tamayito();
		tami2.setNom("kan1");
		tami2.setDescripcion("me gusta el crack");
		tami2.setEtapa(Etapa.Baby);		

		
		session.persist(tami);
		session.persist(tami2);
		
		session.getTransaction().commit();
		
		
		session.getTransaction().begin();
		Tamayito valorant = session.find(Tamayito.class, 1);
		System.out.println(valorant);
		valorant.setNom("valorant");
		session.merge(valorant);
		session.remove(valorant);
		
		session.getTransaction().commit();
		
		session.getTransaction().begin();
		
		
		List<Tamayito> misTamayosjajaja = session.createQuery("from Tamayito").getResultList();
		
		for(Tamayito h : misTamayosjajaja)
		{
			System.out.println(h);
		}
		session.getTransaction().commit();




	}
}
