package act1;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Tamayos")
public class Tamayito {

	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idPowerRanger")
	private int id;
	@Column(name="nom", length=50, nullable=false) 
	private String nom;
	@Column(name="descripcion", length=50, nullable=false) 
	private String descripcion;
	@Column(name="poder", nullable=false, columnDefinition = "double(6,2)")
	private double poder = 50.00;
	@Column(name="viu")
	private boolean viu;
	@Column(name="felicitat")
	private int felicitat = 50;
	@Column(name="etapa", nullable=false)
	private Etapa etapa;
	@Column(name="cumpleanos")
	private LocalDateTime cumple = java.time.LocalDateTime.now();

	
	public Tamayito() {
		super();
	}
	
	public Tamayito(String nom, String descripcion, Etapa etapa) {
		super();
		this.nom = nom;
		this.descripcion = descripcion;
		this.etapa = etapa;
	}


	public Tamayito(String nom, String descripcion, double poder, boolean viu, int felicitat, Etapa etapa,
			LocalDateTime cumple) {
		super();
		this.nom = nom;
		this.descripcion = descripcion;
		this.poder = poder;
		this.viu = viu;
		this.felicitat = felicitat;
		this.etapa = etapa;
		this.cumple = cumple;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getPoder() {
		return poder;
	}

	public void setPoder(double poder) {
		this.poder = poder;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public LocalDateTime getCumple() {
		return cumple;
	}

	public void setCumple(LocalDateTime cumple) {
		this.cumple = cumple;
	}

	@Override
	public String toString() {
		return "Tamayito [id=" + id + ", nom=" + nom + ", descripcion=" + descripcion + ", poder=" + poder + ", viu="
				+ viu + ", felicitat=" + felicitat + ", etapa=" + etapa + ", cumple=" + cumple + "]";
	}

	
}
